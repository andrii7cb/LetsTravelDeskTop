﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace LetsTravelDeskTop
{
    /// <summary>
    /// Interaction logic for UC_CreatePlace.xaml
    /// </summary>
    public partial class UC_CreatePlace : UserControl
    {
        public UC_CreatePlace()
        {
            InitializeComponent();
        }
        private void BackToTripButton_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.contentControl.Content = new UC_CreateTrip();
            this.Content = new UC_CreateTrip();
        }
        private void CreateButton_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.contentControl.Content = new UC_CreateTrip();
            //this.Content = new UC_CreateTrip();
        }
        private void PhotoBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            //MainWindow.contentControl.Content = new UC_CreateTrip();
            //this.Content = new UC_CreateTrip();
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();
            
            if(fileDialog.CheckFileExists && fileDialog.CheckPathExists)
                textBoxPlacePhoto.Text = fileDialog.FileName;
        }
        


    }
}
