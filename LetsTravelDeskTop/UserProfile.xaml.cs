﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LetsTravelDeskTop
{
    /// <summary>
    /// Interaction logic for UserProfile.xaml
    /// </summary>
    public partial class UserProfile : Window
    {
        public UserProfile()
        {
            InitializeComponent();
            ReadUserProfile();
        }

        private void ReadUserProfile()
        {
            textBoxFirstName.Text = "FirstName";
            textBoxMiddleName.Text = "MiddleName";
            textBoxLastName.Text = "LastName";
            textBoxGender.Text = "Gender"; //get a drop down list
            BirthDate.SelectedDate = DateTime.Today;
            textBoxInterestedIn.Text = "list of things";
            textBoxAddress.Text = "1 Main st., Lviv 79000, Ukraine";
            textBoxPhone1.Text = "+1-123-456-7890";
            textBoxPhone2.Text = "+1-123-456-7891";
            textBoxLogin.Text = "Default LogIn";
            textBoxEmail.Text = "some.name@email.com";
            passwordBox1.Password = "password";
            passwordBoxConfirm.Password = "password";
            //throw new NotImplementedException();
        }
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            SaveUserProfile();
            Close();
        }
        private void SaveUserProfile()
        {
            //User tempUser = new WpfApp1.User();
            //tempUser.Show();
            //tempUser.FirstName = textBoxFirstName.Text;

            //throw new NotImplementedException();
        }
    }
}
