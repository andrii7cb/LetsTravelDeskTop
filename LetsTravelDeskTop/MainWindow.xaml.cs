﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LetsTravelDeskTop
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.contentControl.Content = new UC_StartView();
            //this.contentControl.Content = new UC_CreateTrip();            
        }
        private void MenuLogIn_Click(object sender, RoutedEventArgs e)
        {
            Login login = new Login();
            login.Show();
        }
        private void MenuLogOut_Click(object sender, RoutedEventArgs e)
        {
            //Show the message box with the name of the current method
            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");            
        }
        
        private void MenuRegister_Click(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            registration.Show();
        }
        private void MenuRecoverPassword_Click(object sender, RoutedEventArgs e)
        {
            RecoverPassword recover_pass = new RecoverPassword();
            recover_pass.Show();
        }
        private void MenuUserProfile_Click(object sender, RoutedEventArgs e)
        {
            UserProfile user_profile = new UserProfile();
            user_profile.Show();
        }
        private void MenuCheckForUpdates_Click(object sender, RoutedEventArgs e)
        {
            //Show the message box with the name of the current method
            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
        }
        private void MenuAbout_Click(object sender, RoutedEventArgs e)
        {
            //Show the message box with the name of the current method
            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
        }
        private void MenuPreferences_Click(object sender, RoutedEventArgs e)
        {
            //Show the message box with the name of the current method
            MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + " has been activated!");
        }
        private void MenuExit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void BuildTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.contentControl.Content = new UC_CreateTrip();
        }
        private void ChooseTripButton_Click(object sender, RoutedEventArgs e)
        {
            this.contentControl.Content = new UC_ChooseTrip();
        }
    }
}
