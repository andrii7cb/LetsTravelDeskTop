﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LetsTravelDeskTop
{
    /// <summary>
    /// Interaction logic for UC_CreateTrip.xaml
    /// </summary>
    public partial class UC_CreateTrip : UserControl
    {
        public UC_CreateTrip()
        {
            InitializeComponent();

            //file map.html has to be placed into the startup directory
            string borwserPath = "pack://siteoforigin:,,,/map.html";
            Boolean noInternetConnection = false;
            if (noInternetConnection)
                borwserPath = "pack://siteoforigin:,,,/BrowserDefaultDisplay.png";
            this.browser.Navigate(new Uri(borwserPath));            
            browser.BeginInit();
            //browser.Height = MainWindow.ActualHeight;
        }
        private void CreatePlaceButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new UC_CreatePlace();
        }
        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            //to be implemented
        }
        private void BackToStartButton_Click(object sender, RoutedEventArgs e)
        {
            this.Content = new UC_StartView();
        }
        


    }
}
